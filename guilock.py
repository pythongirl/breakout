#!/usr/bin/python2
import sys
import re
from Tkinter import *
import tkFileDialog

class Lock:
    def __init__(self, lock_notation, master_window):
        self.digits = re.search(r'.*(?=\#)', lock_notation).group(0)
        self.digit_range = re.search(r'(?<=\#).*(?=\*)', lock_notation).group(0)
        if self.digit_range == "123":
            self.regex_range = "0-9"
            self.lock_type = "digit"
        elif self.digit_range == "abc":
            self.lock_type = "letter"
            self.regex_range = "a-z"
        else:
            self.lock_type = "unknown"
        self.correct_answer = re.search(r'(?<=\*).*(?=@)', lock_notation).group(0)
        self.name = re.search(r'(?<=@).*', lock_notation).group(0)
        self.user_answer = ""
        self.window = Toplevel()
        self.window.title(self.name)
        self.type = "%s %s lock" % (self.digits, self.lock_type)
        self.status_label = Label(self.window, text="Enter the code:", padx=5, pady=5)
        self.status_label.pack()
        self.entry_box = Entry(self.window)
        self.entry_box.pack()
        self.check_button = Button(self.window, text="Check code", command=self.check)
        self.check_button.pack()
        self.mw_label = Label(master_window, text=self.name)
        self.mw_label.pack()
        self.mw_status_label = Label(master_window, text="unsolved")
        self.mw_status_label.pack()

    def validate_answer(self):
        validator_regex = "^"
        for i in range(int(self.digits)):
            validator_regex += "[%s]" % self.regex_range
        validator_regex += "$"
        return bool(re.match(validator_regex, self.user_answer))

    def check(self):
        self.user_answer = self.entry_box.get()
        self.status_label.config(text=self.is_correct())
        if self.is_correct() == "Correct!":
            self.mw_status_label.config(text="Solved")
        else:
            self.entry_box.delete(0,END)
    
    def is_correct(self):
        if self.validate_answer():
            if self.user_answer == self.correct_answer:
                return "Correct!"
            else:
                return "Wrong!"
        else:
            return "Please enter a vaid code."

if __name__ == "__main__":
    root = Tk()
    f = tkFileDialog.askopenfile(parent=root, title="Choose Breakout file")
    if f != None:
        lock_notations = [x.strip('\n') for x in f.readlines()]
        for i in lock_notations:
            Lock(i, root)

        root.mainloop()
    else:
        Label(root, text="no .bo file was chosen").pack()
        root.mainloop()
