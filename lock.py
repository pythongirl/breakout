#!/usr/bin/python3
import sys
import re
import hashlib

digits = re.search(r'.*(?=\#)', sys.argv[1]).group(0)
digit_range = re.search(r'(?<=\#).*(?=\*)', sys.argv[1]).group(0)
answer_hash = re.search(r'(?<=\*).*(?=@)', sys.argv[1]).group(0)
correct_message = re.search(r'(?<=@).*', sys.argv[1]).group(0)
user_answer = ""

def validate_answer(a):
    validator_regex = "^"
    for i in range(int(digits)):
        validator_regex += "[%s]" % digit_range
    validator_regex += "$"
    return bool(re.match(validator_regex, a))

print("This is ai %s digit lock with a digit range of %s" % (digits, digit_range))
print("What is the answer?")
user_answer = input()

while hashlib.md5(user_answer.encode('utf-8')).hexdigest() != answer_hash:
    if validate_answer(user_answer):
        print("Wrong")
    else:
        print("Please enter a valid answer")
    user_answer = input()

print(correct_message)
